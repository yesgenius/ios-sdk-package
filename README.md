# Setup 

## Adding dependency to your project

### CocoaPods

Add the following line to your podfile

```
pod 'ForceCuBeSDK'
```

### Old school static lib


Download binary *libForceCuBe.a*, resources bundle and header files from our [repo][repo link] and add them to your XCode project.

Go to Target -> Build Phases -> Link Binary With Libraries. Add [libForceCuBe.a][static lib link] to the list.

Add all needed frameworks to the same list:

+ AdSupport
+ CoreBluetooth
+ CoreLocation

Change header search paths accordingly to include ForceCuBe headers directory.

Install *libForceCuBe.a* dependencies:

+ [Reachability](https://github.com/tonymillion/Reachability)
+ [CocoaLumberjack](https://github.com/CocoaLumberjack/CocoaLumberjack)

Go to Target -> Build Phases -> Copy Bundle Resources. Add [ForceCuBeResources.bundle][resources bundle link] to the list.

## Configuring app's modes and permissions

### Turning on Background Modes

Go to Target -> Capabilities -> Background Modes.
Switch on

+ Location updates
+ Uses Bluetooth LE accessories

### Requesting Location Services 'Always' permission

Your app will use user's location info, even when the app is in a background mode. Thus, you will need to include [NSLocationAlwaysUsageDescription](https://developer.apple.com/library/prerelease/ios/documentation/General/Reference/InfoPlistKeyReference/Articles/CocoaKeys.html#//apple_ref/doc/uid/TP40009251-SW18) key into your app's .plist.

#### Proposed values

**ru-RU**

> Нам необходим доступ к информации о вашем местоположении, чтобы
> предоставлять вам наиболее ценные и релевантные предложения о скидках

**en**

> We need to have access to your location, to provide you with the most
> relevant and valuable discount offers

### Requesting permission to show notifications

Your app should request from user a permission to show alerts with badges and sounds.

```
- (BOOL) application: (UIApplication *) application didFinishLaunchingWithOptions: (NSDictionary *) launchOptions
{
    ...
    if ([UIApplication instancesRespondToSelector: @selector (registerUserNotificationSettings:)])
    {
        [application registerUserNotificationSettings: [UIUserNotificationSettings settingsForTypes: UIUserNotificationTypeAlert | UIUserNotificationTypeBadge | UIUserNotificationTypeSound
                                                                                         categories: nil]];
    }
    
    ...
    return YES;
}

```

## Integration demo app


We've built a [demo app](https://gitlab.com/forcecube/ios-sdk-integration-demo) to show case SDK's integration and usage.

# Usage

## Initialisation

Get **APP_DEV_KEY** and **APP_DEV_SECRET** from ForceCuBe. Contact us at so@forcecube.com.

Create an instance of `ForceCuBe` class, passing it **APP_DEV_KEY** and **APP_DEV_SECRET** mentioned above. Also you should pass an externalId, an id which is used to identify a device in your app's analytics / backend systems. It will be used for debugging and data matching purposes.

```
[[ForceCuBe alloc] initWithAppDeveloperKey: @"YOUR APP DEV KEY"
                        appDeveloperSecret: @"YOUR APP DEV SECRET"
                                externalId: @"YOUT EXTERNAL ID HERE"];
```

Now you can call *start* on it to launch SDK initialisation process. When first launched, SDK will need to register new app instance and download server data, containing beacons list and other information.

## Handling SDK state changes and errors

Listen carefully for SDK's changes of state. If you got `FCBStatusStopped`, check `error` code to get the cause of the problem. If you got `FCBStatusStartedWithGeofencing` it means that Bluetooth is off, and SDK will use geofencing to send offers. When state is `FCBStatusStarted`, SDK is using full power mode.

```
- (void) forceCuBe: (ForceCuBe *) fcb didChangeStatus: (FCBStatus) status error: (NSError *) error
{
    if (status == FCBStatusStopped)
    {
        if (error.code == kFCBUserErrorBackgroundLocationTrackingIsOff)
        {
            [self showAlertWithTitle: @"Error" message: @"Access to user location in background is denied."];
        }
        else if (error.code == kFCBNoNetworkOnInitError)
        {
            [self showAlertWithTitle: @"Error" message: @"No network access while initializing"];
        }
        else if (error.domain == kFCBServerErrorDomain)
        {
            [self showAlertWithTitle: @"Error" message: @"Server error while initializing"];
        }
        else if (error.code == kFCBUnrecoverableErrorAuthFailure)
        {
            [self showAlertWithTitle: @"Error" message: @"Wrong dev key or/and dev secret"];
        }
        else if (error.domain == kFCBRegionMonitoringErrorDomain)
        {
            [self showAlertWithTitle: @"Error" message: @"Failed to start location manager, please restart."];
        }
        else if (error.code == kFCBUnrecoverableErrorBLEIsUnsupported)
        {
            [self showAlertWithTitle: @"Error" message: @"BLE is not supported on this device"];
        }
        else if (error.code == kFCBUnrecoverableErrorBLEAccessIsUnauthorized)
        {
           [self showAlertWithTitle: @"Error" message: @"App doesnt have permission to access BLE"];
        }
    }
    else if (status == FCBStatusStartedWithGeofencing)
    {
        [self showAlertWithTitle: @"Warning" message: @"Bluetooth is off."];
    }
}
```

See detailed description of SDK's error domains and errors in [FCBErrors.h](https://gitlab.com/forcecube/ios-sdk-package/blob/master/Headers/FCBErrors.h).

## Offer delivery

SDK will trigger a callback `manager:didDeliverCampaignOffer:`, when phone is at the right place.
Callback can be triggered either by GPS-based geofencing or by BLE advertising packets.

Your app should set a proper delegate for FCBCampaignManager object.

```
- (void) manager: (id<FCBCampaignManager>) campaignManager
didDeliverCampaignOffer: (id<FCBCampaignOffer>) campaignOffer
{
    UILocalNotification * ln = [UILocalNotification new];
    
    ln.alertBody = [campaignOffer.notificationText stringByReplacingOccurrencesOfString: @"%" withString: @"%%" options: 0  range: NSMakeRange(0, campaignOffer.notificationText.length)];
    ln.soundName = UILocalNotificationDefaultSoundName;
    ln.userInfo = @{ kNotificationCampaignIdKey: @(campaignOffer.campaignOfferId)};
    ln.applicationIconBadgeNumber = campaignManager.unopenedOffers.count;
    
    [[UIApplication sharedApplication] presentLocalNotificationNow: ln];
}
```

You should escape '%' in offer's notificationText and set userInfo of notification to handle it correctly later in

```
-(void) application: (UIApplication *) application didReceiveLocalNotification: (UILocalNotification *) notification
{
    if (notification.userInfo[kNotificationCampaignIdKey])
    {
        NSUInteger campaignId = [notification.userInfo[kNotificationCampaignIdKey] integerValue];
        
        // Show offer controller
    }    
}
```

You can get offer details via `campaignOfferById:` method of `FCBCampaignManager` instance.

After the notification is sent, the campaign offer status is set to `kFCBCampaignStatusPresented` and it is put into `unopenedOffers` list.

After that, a user can save the offer for later redemption, in this case call `setCampaignOfferAsAccepted:` or decline it (call `setCampaignOfferAsDeclined:`).

If user accepts the offer, it will be added into *acceptedOffers* collection. You should use this collection to show a list of accepted offers to a user.

## Getting a list of offers accepted by user

You should use `acceptedOffers` collection to show a list of offers, accepted by the user. Every item in the collection supports `FCBCampaignOffer` protocol.

## Showing offer details and managing redemption process

First, you should obtain `FCBDataWriter` instance from ForceCuBe.

When user pushes 'Redeem' button or starts a redemption process in any other way, the app should set up data writing context properly. 

```
dataWriter.campaignOfferId = campaignOfferId;
dataWriter.delegate = self;

```

You must also implement a delegate for `FCBDataWriter`.


```
- (void) dataWriterDidStartDataWriting: (id<FCBDataWriter>) dataWriter
{
    // Show activity indicator to the user
}

- (void) dataWriter: (id<FCBDataWriter>) dataWriter didFailToStartWritingWithError: (NSError *) error
{
    // Something bad happend, show error

    // Clear data writing context
    dataWriter.campaignOfferId = kFCBDataWriterCampaignOfferIdNotSet;
    dataWriter.delegate = nil;
}

- (void) dataWriter: (id<FCBDataWriter>) dataWriter didFinishWritingWithError: (NSError *) error
{
    if (error)
    {
        // Something bad happend, show error
    }
    else
    {
        // Everything went fine, yay!
    }

    // Clear data writing context
    dataWriter.campaignOfferId = kFCBDataWriterCampaignOfferIdNotSet;
    dataWriter.delegate = nil;
}
```

See detailed description of data writer error domains and error codes in [FCBDataWriterErrors.h](https://gitlab.com/forcecube/ios-sdk-package/blob/master/Headers/FCBDataWriterErrors.h).

If user redeems the coupon, it will be removed from the *acceptedOffers* list.

## Offer reminders

When a user enters specific location, where he/she can use offers, which are already accepted, SDK will send a callback `manager:hasAcceptedOffers:forCurrentLocation:advertisers`. Callback carries with it several params: a list of offers, that can be used neraby, 
current location (with name), a list of advertisers to which the offers 
A typical code to handle this callback is shown below

```
- (void) manager: (id<FCBCampaignManager>)campaignManager hasAcceptedOffers: (NSArray<id<FCBCampaignOffer>> *)campaignOffers forCurrentLocation:(id<FCBLocation>)location advertisers: (NSArray<id<FCBAdvertiser>> *) advertisers
{
    UILocalNotification * ln = [UILocalNotification new];
    
    NSString * names = [[advertisers valueForKey: @"name"] componentsJoinedByString: @","];
    
    ln.alertBody = [NSString stringWithFormat: @"You have saved offers in %@ from %@", location.name, names];
    ln.soundName = UILocalNotificationDefaultSoundName;
    
    [[UIApplication sharedApplication] presentLocalNotificationNow: ln];
}
```

## Runtime settings

Runtime settings include:
- Log level
- Server poll mode
- Accepted offer reminder policy

An app developer can alter those to make integration easier. Server poll mode determines how often the SDK will go for server data (that is how quickly will it be able to pick up new offers from the FCB server). Reminder policy determines reminder's  fire conditions - time elapsed since the offer was accepted and allowed frequency of reminders.

[repo link]:https://gitlab.com/forcecube/ios-sdk-package
[static lib link]:https://gitlab.com/forcecube/ios-sdk-package/blob/master/libForceCuBe.a
[resources bundle link]:https://gitlab.com/forcecube/ios-sdk-package/tree/master/ForceCuBeResources.bundle