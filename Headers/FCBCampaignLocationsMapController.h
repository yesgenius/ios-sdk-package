//
//  FCBCampaignLocationsMapController.h
//  ForceCuBe
//
//  Created by Stanislav Olekhnovich on 28/06/16.
//  Copyright © 2016 Stanislav Olekhnovich. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@class ForceCuBe;

@interface FCBCampaignLocationsMapController : UIViewController

@property ForceCuBe * forcecube;
@property NSUInteger campaignOfferId;

@property IBOutlet MKMapView * mapView;

@end
