//
//  FCBLocationProtocol.h
//  ForceCuBe
//
//  Created by Stanislav Olekhnovich on 18/07/16.
//  Copyright © 2016 Stanislav Olekhnovich. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 Location types.
 */
typedef NS_ENUM(NSInteger, FCBLocationType)
{
    /**
     A mall. Has GPS coordinates and address.
     */
    kFCBLocationTypeMall,
    /**
     A shop, located inside of the mall. Has no separate address or GPS coordinates.
     */
    kFCBLocationTypeMallShop,
    /**
     A shop, located on a street. Has GPS coordinates and address.
     */
    kFCBLocationTypeStreetShop,
};

/**
 Location
 */
@protocol FCBLocation <NSObject>

/**
 */
@property (readonly) NSUInteger locationId;
/**
 Location type, see FCBLocationType
 */
@property (readonly) FCBLocationType type;
/**
 Location friedly name. Is set by FORCECUBE's portal user.
 */
@property (readonly) NSString * name;
/**
 Phone number.
 */
@property (readonly) NSString * phone;
/**
 Working hours of a mall or a street shop. Not set for a mall shop.
 */
@property (readonly) NSString * workingHours;
/**
 Address a mall or a street shop. Not set for a mall shop.
 */
@property (readonly) NSString * address;
/**
 Latitude of a mall or a street shop. Not set for a mall shop.
 */
@property (assign, readonly) double latitude;
/**
 Longitude of a mall or a street shop. Not set for a mall shop.
 */
@property (assign, readonly) double longitude;
/**
 Id of a parent mall. Set for mall shop. Not set for malls and street shops.
 */
@property (assign, readonly) NSUInteger parentId;
/**
 Indoor directions for a mall shop to giud customers inside the mall. Set for mall shop. Not set for malls and street shops.
 */
@property (readonly) NSString * indoorDirections;
/**
 A link to a map, depicting indoor location of the shop (typically a link to a 2GIS service)
 */
@property (readonly) NSString * directionsUrl;

@end
