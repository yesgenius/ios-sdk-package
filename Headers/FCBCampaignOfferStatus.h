//
//  FCBCampaignOfferStatus.h
//  ForceCuBe
//
//  Created by Stanislav Olekhnovich on 02/08/16.
//  Copyright © 2016 Stanislav Olekhnovich. All rights reserved.
//

/**
 Campaign offer statuses.
 */
typedef NS_ENUM(NSUInteger, FCBCampaignStatus)
{
    /**
     Initial status.
     */
    kFCBCampaignStatusInitial = 0,
    
    /**
     Campaign offer was delivered via callback or notification.
     */
    kFCBCampaignStatusDelivered,
    
    /**
     Campaign offer was viewed by a user.
     */
    kFCBCampaignStatusOpened,
    
    /**
     Campaign offer was accepted, saved.
     */
    kFCBCampaignStatusAccepted,
    
    /**
     Campaign offer was declined.
     */
    kFCBCampaignStatusDeclined,
    
    /**
     Campaign offer was redeemed.
     */
    kFCBCampaignStatusRedeemed,
    
    /**
     Campaign offer was expired or invalidated by advertiser.
     */
    kFCBCampaignStatusInvalidated
};