//
//  FCBAdvertisersFilterManagerProtocol.h
//  ForceCuBe
//
//  Created by Stanislav Olekhnovich on 19/07/16.
//  Copyright © 2016 Stanislav Olekhnovich. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "FCBAdvertiserProtocol.h"

/**
 API for managing filtering ad, based on user brand preferences.
 */
@protocol FCBAdvertisersFilterManager <NSObject>

/**
 A list of all advertisers, who can send their ads within tha app. Subscribe to this property to receive updates.
 */
@property (readonly) NSArray<id<FCBAdvertiser>> * advertisers;

/**
 Allowes of disallows advertisers to send ads.
 
 @param advertiserId An advertiser id
 @param isEnabled
 */
- (void) setAdvertiserWithId: (NSUInteger) advertiserId enabled: (BOOL) isEnabled;


/**
 Refresh a list a advertisers.
 */
- (void) refresh;

@end
