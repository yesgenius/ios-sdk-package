//
//  FCBNotificationSettingsProtocol.h
//  ForceCuBe
//
//  Created by Stanislav Olekhnovich on 19/07/16.
//  Copyright © 2016 Stanislav Olekhnovich. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 API for managing notifications frequency.
 */
@protocol FCBNotificationSettingsProtocol <NSObject>

/**
 Interval, which is used as base for all calculations.
 By default, it's 7 days.
 */
@property NSTimeInterval baseInterval;

/**
 Max number of offer notifications during <see>baseInterval</see>
 By default, its 2.
 */
@property NSUInteger maxNotificationsCount;

/**
 Can not be larger than <>targetInterval</>
 By defualt its 30 mins.
 */
@property NSTimeInterval minIntervalBetweenNotifications;

@end
