//
//  FCBDelegate.h
//  ForceCuBe
//
//  Created by Stanislav Olekhnovich on 24/12/14.
//  Copyright (c) 2014 Stanislav Olekhnovich. All rights reserved.
//

#import "FCBStatus.h"

@class ForceCuBe;

/**
 The FCBDelegate protocol defines the methods used to receive BLE beacons related events from ForceCuBe object.
 */
@protocol FCBDelegate <NSObject>

/**
 Tells a delegate, that a ForceCuBe object has changed its status.
 
 @param fcb The ForceCuBe object reporting the event.
 @param newStatus Shows if ForceCuBe SDK is started, starting or stopped.
 @param error If status is FCBStatusStopped, then error's code should be checked
 */
- (void) forceCuBe: (ForceCuBe *) fcb
   didChangeStatus: (FCBStatus) newStatus
             error: (NSError *) error;

/**
 Tells a delegate, that ForceCuBe SDK has encountered a error, which it can not handle properly and which can cause it to malfunction.
 
 @param fcb The ForceCuBe object reporting the event.
 @param error Error, which can affect SDK's performance.
 */
- (void) forceCuBe: (ForceCuBe *) fcb didRunIntoError: (NSError *) error;

@end
