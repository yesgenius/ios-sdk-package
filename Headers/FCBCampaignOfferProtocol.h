//
//  FCBDigitalEntity.h
//  ForceCuBe
//
//  Created by Stanislav Olekhnovich on 21/01/16.
//  Copyright © 2016 Stanislav Olekhnovich. All rights reserved.
//

#import "FCBCampaignOfferStatus.h"

/** The FCBCampaignOffer protocol 
 */
@protocol FCBCampaignOffer<NSObject>

/**
 Campaign offer identifier.
 */
@property (readonly) NSUInteger campaignOfferId;

/**
 Offer title. Contains compact description of the offer.
 */
@property (readonly) NSString * notificationText;

/**
 Offer title. Contains compact description of the offer.
 */
@property (readonly) NSString * fullscreenTitle;

/**
 Description, that is used to provide details of the offer (validity period, places ,where coupon can be checked in)
 */
@property (readonly) NSString * fullscreenText;

/**
 Offer image url.
 */
@property (readonly) NSURL * imageUrl;

/**
 Offer avalibility period start.
 */
@property (readonly) NSDate * avaliableFrom;

/**
 Offer avalibility period end.
 */
@property (readonly) NSDate * avaliableTo;

/**
 Campaign offer status.
 */
@property (readonly) FCBCampaignStatus status;

/**
 Ide of the advertiser.
 */
@property (readonly) NSUInteger advertiserId;


@end
