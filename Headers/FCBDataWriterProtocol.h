//
//  FCBDataWriterProtocol.h
//  ForceCuBe
//
//  Created by Stanislav Olekhnovich on 19/07/16.
//  Copyright © 2016 Stanislav Olekhnovich. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSUInteger const kFCBDataWriterCampaignOfferIdNotSet;

@protocol FCBDataWriter;

/**
 A FCBDataWriter's delegate.
 */
@protocol FCBDataWriterDelegate <NSObject>

/**
 Tells a delegate that a process of data writing did start.
 
 @param dataWriter A data writer.
 */
- (void) dataWriterDidStartDataWriting: (id<FCBDataWriter>) dataWriter;

/**
 Tells a delegate that a process of data writing did fail to start.
 
 @param dataWriter A data writer.
 @param error Error, which causes data writing to fail.
 */
- (void) dataWriter: (id<FCBDataWriter>) dataWriter didFailToStartWritingWithError: (NSError *) error;

/**
 Tells a delegate that a process of data writing did finish.
 
 @param dataWriter A data writer.
 @param error If not nil, then data writing ended in failure.
 */
- (void) dataWriter: (id<FCBDataWriter>) dataWriter didFinishWritingWithError: (NSError *) error;

@end

/**
 API for FORCECUBE's BLE data writing capabilities.
 */
@protocol FCBDataWriter <NSObject>

/**
 A delegate.
 */
@property (weak) id<FCBDataWriterDelegate> delegate;

/**
 When a smartphone is close enough to a FCBReader, data writer checks
 this property to decide what data to send to the FCBReader. 
 It writes POS-code of offer with this id.
 */
@property (assign) NSUInteger campaignOfferId;

@end
