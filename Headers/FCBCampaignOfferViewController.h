//
//  FCBCampaignContentViewController.h
//  ForceCuBe
//
//  Created by Stanislav Olekhnovich on 13/01/16.
//  Copyright © 2016 Stanislav Olekhnovich. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FCBCampaignOfferViewController, ForceCuBe;

/**
 The FCBCampaignOfferViewControllerDelegate protocol defines callbacks received 
 from FCBCampaignOfferViewController object.
 */
@protocol FCBCampaignOfferViewControllerDelegate <NSObject>

/**
 Tells a delegate, that a FCBCampaignOfferViewController has fulfilled it's duty and should be dismissed.
 
 @param controller The FCBCampaignOfferViewController object
 @param wasOfferAccepted Indicates if offer was accepted by the user and
 thus was inserted into ForceCuBe's acceptedOffers list.
 */
- (void) offerViewControllerDidComplete: (FCBCampaignOfferViewController *) controller
                      withOfferAccepted: (BOOL) wasOfferAccepted;

@end

/**
  The FCBCampaignOfferViewController class presents a full view of a campaign offer.
 */
@interface FCBCampaignOfferViewController : UIViewController

/**
 Id of a campaign, which offer will be presented by the controller.
 Must be set before showing controller to a user.
 */
@property NSUInteger campaignOfferId;
/**
 ForceCuBe object, which is used to fetch campaign offer info.
 Must be set before showing controller to a user.
 */
@property ForceCuBe * forcecube;

/**
 A delegate.
 */
@property id<FCBCampaignOfferViewControllerDelegate> delegate;



@end
