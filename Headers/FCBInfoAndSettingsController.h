//
//  FCBInfoAndSettingsController.h
//  ForceCuBe
//
//  Created by Stanislav Olekhnovich on 12/04/16.
//  Copyright © 2016 Stanislav Olekhnovich. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ForceCuBe;

@interface FCBInfoAndSettingsController : UITableViewController

@property ForceCuBe * forcecube;

- (IBAction) backButtonPressed: (id) sender;

@end
