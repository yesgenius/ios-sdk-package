//
//  FCBRuntimeSettings.h
//  ForceCuBe
//
//  Created by Stanislav Olekhnovich on 08/04/16.
//  Copyright © 2016 Stanislav Olekhnovich. All rights reserved.
//

/**
 Log levels for ForceCuBeSDK.
 */
typedef NS_ENUM(NSUInteger, FCBLogLevel)
{
    /**
     No logs.
     */
    kFCBLogLevelOff = 0,
    
    /**
     Error logs only.
     */
    kFCBLogLevelError,
    
    /**
     Error and warning logs.
     */
    kFCBLogLevelWarning,
    
    /**
     Error, warning and info logs.
     */
    kFCBLogLevelInfo,
    
    /**
     Error, warning, info and debug logs.
     */
    kFCBLogLevelDebug,
    
    /**
     Error, warning, info, debug and verbose logs.
     */
    kFCBLogLevelVerbose
};

/**
 Server poll modes.
 */
typedef NS_ENUM(NSUInteger, FCBServerPollMode)
{
    /**
     Poll every 2 mins.
     */
    kFCBServerPollModeTest = 0,
    
    /**
     One poll every 24h. Default.
     */
    kFCBServerPollModeProduction
};

/**
 Accpeted offer reminder modes.
 */
typedef NS_ENUM(NSUInteger, FCBAcceptedOfferReminderPolicy)
{
    /**
     Reminds unlimited number of times. No pause between acceptance and reminder.
     */
    kFCBAcceptedOfferReminderPolicyTest = 0,
    
    /**
     Reminds once. Min 48h between acceptance and reminder.
     */
    kFCBAcceptedOfferReminderPolicyProduction
};

/**
 SDK's runtime settings container.
 */
@protocol FCBRuntimeSettings <NSObject>

/**
 SDK's log level. Default is kFCBLogLevelError.
 */
@property (assign, nonatomic) FCBLogLevel logLevel;

/**
 Server poll mode. Default is kFCBServerPollModeProduction.
 */
@property (assign, nonatomic) FCBServerPollMode serverPollMode;

/**
 Accpeted offer reminder policy. Default is kFCBAcceptedOfferReminderPolicyProduction.
 */
@property (assign, nonatomic) FCBAcceptedOfferReminderPolicy acceptedOfferReminderPolicy;

@end
