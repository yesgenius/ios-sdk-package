//
//  FCBErrors.h
//  ForceCuBe
//
//  Created by Stanislav Olekhnovich on 29/07/15.
//  Copyright (c) 2015 Stanislav Olekhnovich. All rights reserved.
//

extern NSString * const kFCBInternalInconsistencyException;

/**
 ForceCuBe SDK's no network on init error domain.
 */
extern NSString * const kFCBNoNetworkOnInitErrorDomain;

/**
 SDK needs network access at the first launch.
 The SDK will try to init again, when
 network will be available.
 */
extern const NSInteger kFCBNoNetworkOnInitError;

/**
 ForceCuBe SDK's disk error.
 */
extern NSString * const kFCBDiskErrorDomain;

typedef NS_ENUM(NSInteger, FCBDiskError)
{
    /**
     Disk is full. This means SDK is not able to cache the server data and will use network more often.
     */
    kFCBDiskErrorNotEnoughDiskSpace,
    
    /**
     Error writing/reading or moving a file. This means SDK is not able to cache the server data and will use network more often.
     */
    kFCBDiskErrorFileSystemError
};

/**
 ForceCuBe SDK's region monitoring error domain. Tha app should restart the SDK.
 */
extern NSString * const kFCBRegionMonitoringErrorDomain;

typedef NS_ENUM(NSInteger, FCBRegionMonitoringError)
{
    /**
     SDK did try to start CLRegion monitoring, but failed. Most likely the app is monitoring all 20 regions allowed.
     */
    kFCBRegionMonitoringErrorFailedToStartRegionMonitoring,
    /**
     LocationManager did fail, SDK can not monitor CLRegions no more.
     */
    kFCBRegionMonitoringErrorLocationManagerDidFail
};

/**
 ForceCuBe SDK's server domain. Something is wrong with the FORCECUBE backend. Error will be cleared,
 when the backend recovers.
 */
extern NSString * const kFCBServerErrorDomain;

typedef NS_ENUM(NSInteger, FCBServerError)
{
    /**
     ForceCuBe servers reply with 5xx HTTP status codes, when SDK tries to init.
     */
    kFCBServerErrorServerIsDown,
    /**
     ForceCuBe servers are sending unexpected HTTP response codes (4xx), when SDK tries to init.
     */
    kFCBServerErrorUnexpectedStatusCode,
    /**
     ForceCuBe servers are sending incorrect json, when SDK tries to init.
     */
    kFCBServerErrorWrongJsonFormat
};


/**
 ForceCuBe SDK's unrecoverable errors domain. It's not possible to recover from
 these errors in runtime.
 */
extern NSString * const kFCBUnrecoverableErrorDomain;

typedef NS_ENUM(NSInteger, FCBUnrecoverableError)
{
    /**
     Device on which the app and SDK are running does not support BLE.
     */
    kFCBUnrecoverableErrorBLEIsUnsupported,
    /**
     Most likely app has to provide a certain key in Info.plist.
     */
    kFCBUnrecoverableErrorBLEAccessIsUnauthorized,
    
    /**
     Incorrect appDevKey or/and appDevSecret are provided to SDK.
     */
    kFCBUnrecoverableErrorAuthFailure
};


/**
 ForceCuBe SDK's user domain. User actions are required to recover.
 */
extern NSString * const kFCBUserErrorDomain;

typedef NS_ENUM(NSInteger, FCBUserError)
{
    /**
     User haven't authorized app to use his location in background. Or user has switched off Location Services altogether. When user authorizes app to track location in background or switches on Location Services, SDK will try to start again.
     */
    kFCBUserErrorBackgroundLocationTrackingIsOff
};
