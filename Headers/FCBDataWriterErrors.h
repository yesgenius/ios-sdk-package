//
//  FCBDataWriterErrors.h
//  ForceCuBe
//
//  Created by Stanislav Olekhnovich on 31/03/16.
//  Copyright © 2016 Stanislav Olekhnovich. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 FORCECUBE's data writer errors domain.
 */
extern NSString * const kFCBDataWriterErrorDomain;

/**
 FORCECUBE's data writer errors.
 */
typedef NS_ENUM(NSInteger, FCBDataWriterError)
{
    /**
     Something went wrong, when communicating with FCBReader. App shold retry data writing.
     */
    kFCBDataWriterErrorDeviceCommunicationError,
    /**
     App has provided a id of an offer that can not be redeemed in the location, where the FCBReader is installed.
     */
    kFCBDataWriterErrorWrongLocation,
    /**
     App has provided a id of an offer, which is not in FCBCampaignStatusAccepted.
     */
    kFCBDataWriterErrorWrongOfferStatus,
    /**
     Something went wrong when SDK tried to encrypt/descrypt messages to/from the FCBReader.
     */
    kFCBDataWriterErrorEncryptionError,
    /**
     SDK doesn't have encryption keys to establish secure communication with the FCBReader.
     */
    kFCBDataWriterNoEncryptionKeysSetError
};
