Pod::Spec.new do |s|
    s.name         = "ForceCuBeSDK"
    s.version      = "2.0.11"

    s.summary      = "ForceCuBe SDK for beacon-based proximity marketing campaigns."
    s.homepage     = "http://forcecube.com"
    s.author       = { "ForceCuBe" => "so@forcecube.com" }

    s.platform     = :ios
    s.ios.deployment_target = '8.0'

    s.source       = {  :git => "https://gitlab.com/forcecube/ios-sdk-package.git",
                        :tag => s.version.to_s }
    s.source_files =  'Headers/*.h'
    s.vendored_libraries = 'libForceCuBe.a'
    s.resource =  'ForceCuBeResources.bundle'

    s.frameworks = 'UIKit', 'Foundation', 'AdSupport', 'CoreBluetooth', 'CoreLocation'

    s.dependency 'Reachability'
    s.dependency 'CocoaLumberjack', '~> 2.3.0'

    s.requires_arc = true
    s.xcconfig  =  { 'HEADER_SEARCH_PATHS' => '"${PODS_ROOT}/Headers/ForceCuBeSDK"' }

    s.license      = {
        :type => 'Copyright',
        :text => <<-LICENSE
            Copyright 2015 ForceCuBe. All rights reserved.
            LICENSE
    }

end
